const path = require("path")
const webpack = require("webpack");

module.exports = {
	entry: [
		"babel-polyfill",
		"./js/src/Main.js"
	],
	output: {
		path: path.resolve(__dirname, "./js/dst/"),
		publicPath: "/js/dst",
		filename: "bundle.js"
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: "vue-loader",
				options: {
					loaders: {
						"scss": "vue-style-loader!css-loader!sass-loader?sourceMap",
						"sass": "vue-style-loader!css-loader!sass-loader?indentedSyntax",
					}
				}
			},
			{
				test: /\.js$/,
				loader: "babel-loader",
				exclude: /node_modules/,
				query: {
					presets: ["es2015", "flow-vue"]
				}
			},
			{
				test: [/\.worker.js$/],
				loader: "worker-loader?inline!babel?presets=es2015",
			}
		]
	},
	plugins: [
		new webpack.LoaderOptionsPlugin({
			options: {
				sassLoader: {
					includePaths: [path.resolve(__dirname, "src", "scss")]
				},
				worker: {
					output: {
						filename: "./js/dst/hash.worker.js",
						chunkFilename: "./js/dst/[id].hash.worker.js"
					}
				},
				context: "/"
			}
		}),
	],
	resolve: {
		alias: {
			"vue$": "vue/dist/vue.common.js"
		},
		extensions: [".js", "worker.js", ".vue"]
	},
	devServer: {
		historyApiFallback: true,
		noInfo: true
	},
	performance: {
		hints: false
	},
	devtool: "#source-map"
}

if (process.env.NODE_ENV === "production") {
	module.exports.devtool = "#source-map"
	module.exports.plugins = (module.exports.plugins || []).concat([
		new webpack.DefinePlugin({
			"process.env": {
				NODE_ENV: '"production"'
			}
		}),
		new webpack.optimize.UglifyJsPlugin({
			sourceMap: true,
			compress: {
				warning: false
			}
		}),
		new webpack.LoaderOptionsPlugin({
			minimize: true
		})
	])
}
