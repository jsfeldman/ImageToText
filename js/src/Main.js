// @flow

import Vue from "vue";
import inputPicker from "./InputPicker"

class Main {

	static main(...args: Array<string>) {
		new Vue({
			el: "#app",
			components: { inputPicker }
		});
	}
}

window.onload = () => {
	Main.main();
};
